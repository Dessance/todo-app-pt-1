import React from 'react'


function TodoItem ({todo, toggleItem, deleteTodo}) {
  const {completed, id, title} = todo
  return (
    <>
    <li className={todos.completed ? "completed" : ""}>
      <div className="view">
        <input 
        className="toggle" type="checkbox" 
        checked={completed} 
        onClick={() => toggleItem(id)} />
        <label>{title}</label>
        <button className="destroy" onClick={() => deleteTodo(id)} />
      </div>
    </li>
   </> 
  );
}
export default TodoItem

