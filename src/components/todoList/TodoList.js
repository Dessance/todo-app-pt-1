import React from 'react'
import TodoItem from '../todoItem/TodoItem'


const TodoList = (props) => {
  // const TodoId= id => <li key={uuid()}>{id}</li>
  return (
    <section className="main">
    <ul className="todo-list">
      {props.todos.map((todo, i) => (
        // console.log(todo),
        <TodoItem 
        title={todo.title} 
        deleteTodo={deleteTodo}
        todo={todo}
        completed={todo.completed} 
        key={i} 
        toggleItem ={toggleItem} 
        />
        ))}
    </ul>
  </section>
);
}

export default TodoList
    

