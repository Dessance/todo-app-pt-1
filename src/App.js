//worked in a few study groups and help from Gabby
import React, { useState } from "react";
import todosList from "./todos.json";
// import uuid from 'react-uuid'


// I need to save this
// state = {
//   todos: todosList,
// };
function App() { 
  const [todos, setTodos] = useState(todosList)
  const [title, setTitle] = useState("")

  // function TodoItem ({todo, toggleItem, deleteTodo}) {
  //   const {completed, id, title} = todo
  //   return (
  //     <>
  //   {/* <li key={todos.id}>{todos.title}</li> */}
  //     <li className={todos.completed ? "completed" : ""}>
  //       <div className="view">
  //         <input 
  //         className="toggle" type="checkbox" 
  //         checked={completed} 
  //         onClick={() => toggleItem(id)} />
  //         {/* {console.log(props.title)} */}
  //         <label>{title}</label>
  //         <button className="destroy" onClick={() => deleteTodo(id)} />
  //       </div>
  //     </li>
  //    </> 
  //   );
  // }
  
//   const TodoList = (props) => {
//     // const TodoId= id => <li key={uuid()}>{id}</li>
//     return (
//       <section className="main">
//       <ul className="todo-list">
//         {props.todos.map((todo, i) => (
//           // console.log(todo),
//           <TodoItem 
//           title={todo.title} 
//           deleteTodo={deleteTodo}
//           todo={todo}
//           completed={todo.completed} 
//           key={i} 
//           toggleItem ={toggleItem} 
//           />
//           ))}
//       </ul>
//     </section>
//   );
// }

function addTodo(task) {
  const newTodo =  {
    userId: 1,
    id: uuid(),
    title: task,
    completed: false,
  };
  setTodos([...todos, newTodo]);
}

const handleOnChange = e => {
 setTitle(e.target.value);
  
};

const handleSubmit = (e) => {
  e.preventDefault();
  addTodo(title);
  setTitle("");
}

function deleteTodo(id){
 console.log("Click Me HERE DELETE")
 const updatedTodos = todos.filter((todo) => {
  const toKeepItem = id !== todo.id;
  return toKeepItem;
});
setTodos(updatedTodos);
 
}

function toggleItem(id) {
  const updatedTodos = todos.map((todo) => {
    if (id === todo.id) {
      return { ...todo, completed: !todo.completed };
    }
    return todo;
  });
  setTodos(updatedTodos);
}


function clearCompleted() {
  const updatedTodos = todos.filter((todo) => {
    const toKeepItem = !todo.completed;
    return toKeepItem;
  });
  setTodos(updatedTodos);
}

    return (
      <section className="todoapp">
        <header className="header">
          <h1>todos</h1>
        <form onSubmit={handleSubmit} >
          <input className="new-todo"  type="text" value={title} onChange={handleOnChange} placeholder="New Todo Here:"  />
        </form>
        </header>
        <TodoList todos={todos} toggleItem={toggleItem} deleteTodo={deleteTodo} />
        <footer className="footer">
          <span className="todo-count">
            <strong>0</strong> item(s) left
          </span>
          <button type="submit" className="clear-completed"  onClick = {() => clearCompleted()}>Clear completed</button>
        </footer>
      </section>
    );
  }
  
  export default App;

